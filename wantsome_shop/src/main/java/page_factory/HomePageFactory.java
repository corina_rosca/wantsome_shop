package page_factory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_objects.SearchResultPage;

public class HomePageFactory {
    private WebDriver driver;

    public HomePageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".search-icon")
    @CacheLookup
    WebElement searchIcon;

    @FindBy(css = ".search-user-block [placeholder]")
    @CacheLookup
    WebElement searchIconTextBox;

    @FindBy(css = ".search-user-block button")
    @CacheLookup
    WebElement searchSubmit;


    @FindBy(css = ".fa-user-times")
    @CacheLookup
    WebElement userIcon;

    @FindBy(css = ".category-toggle")
    @CacheLookup
    WebElement dropdownCategory;

    @FindBy(css = "[id='menu-item-509'] [href]")
    @CacheLookup
    WebElement menCollectionCategory;

    @FindBy(css = "[id='menu-item-510'] [href]")
    @CacheLookup
    WebElement womenCollectionCategory;


    public String checkHomePageTitle() {
        return driver.getTitle();
    }

    public MyAccountPageFactory navigateToLoginOrRegisterPage() {
        userIcon.click();
        return new MyAccountPageFactory(driver);
    }

    public void navigateToMenCollection() {
        dropdownCategory.click();
        WebDriverWait wait = new WebDriverWait(driver, 7);
        wait.until(ExpectedConditions.elementToBeClickable(menCollectionCategory));
        menCollectionCategory.click();
    }

    public void navigateToWomenCollection() {
        dropdownCategory.click();
        WebDriverWait wait = new WebDriverWait(driver, 7);
        wait.until(ExpectedConditions.elementToBeClickable(womenCollectionCategory));
        womenCollectionCategory.click();
    }

    public void clickOnSearchIcon() {
        searchIcon.click();
    }

    public void typeSearch(String searchInput) {
        searchIconTextBox.sendKeys(searchInput);
        searchSubmit.sendKeys(Keys.ENTER);
        new SearchResultPage(driver);
    }


}





