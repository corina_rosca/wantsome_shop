package page_factory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CheckOutPageFactory {
    private final WebDriver driver;

    public CheckOutPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".entry-title")
    @CacheLookup
    WebElement pageTitle;

    @FindBy(css = ".showlogin")
    @CacheLookup
    WebElement loginLink;

    @FindBy(css = "[for='username']")
    @CacheLookup
    WebElement usernameLabel;

    @FindBy(css = "[for='password']")
    @CacheLookup
    WebElement currentPasswordLabel;


    @FindBy(css = ".showcoupon")
    @CacheLookup
    WebElement couponCodeLink;

    @FindBy(css = "[placeholder='Coupon code']")
    @CacheLookup
    WebElement couponCodePlaceholder;

    @FindBy(css = ".woocommerce-error li")
    WebElement couponErrorMsg;

    @FindBy(id = "place_order")
    @CacheLookup
    WebElement placeOrderButton;

    @FindBy(css = ".woocommerce-error")
    List<WebElement> checkoutError;


    public String getPageTitle() {
        return pageTitle.getText().toUpperCase();
    }

    public void getLoginForm() {
        loginLink.click();
    }

    public String getUsernameLabel() {
        return usernameLabel.getText();
    }

    public String getCurrentPassLabel() {
        return currentPasswordLabel.getText();
    }

    public void getHaveACouponCode() {
        couponCodeLink.click();
    }

    public void getCouponInputText() {
        couponCodePlaceholder.sendKeys("123Ab");
        couponCodePlaceholder.sendKeys(Keys.ENTER);
    }

    public String getCouponErrorMsg() {
        return couponErrorMsg.getText();
    }

    public void placeTheOrder() {
        placeOrderButton.click();
    }

    public String getCheckOutError() {
        for (WebElement error : checkoutError) {
            return error.getText();
        }

        return checkoutError.toString();
    }


}
