package page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SearchResultPageFactory {
    private WebDriver driver;

    public SearchResultPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "[id ='primary'] [class= 'entry-title']")
    @CacheLookup
    List<WebElement> searchResult;

    @FindBy(css = "div[id='primary']>article")
    @CacheLookup
    List<WebElement> resultNo;

    public Boolean checkSearchResult(String searchInput) {
        List<WebElement> searchResults = searchResult;
        for (WebElement result : searchResults) {
            if (!result.getText().toLowerCase().contains(searchInput)) {
                return false;
            }
        }

        return true;
    }

    public int checkResultNo(String searchInput) {
        List<WebElement> resultNo = searchResult;
        for (WebElement result : resultNo) {
            if (!result.getText().toLowerCase().contains(searchInput)) {
                return Integer.parseInt(result.getText());
            }
        }
        return resultNo.size();
    }
}





