package page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ProductsDetails {
    private WebDriver driver;


    public ProductsDetails(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(css = ".product_title")
    @CacheLookup
    WebElement productTitle;

    @FindBy(css = ".posted_in")
    @CacheLookup
    WebElement productCategory;

    @FindBy(css = ".description_tab ")
    @CacheLookup
    WebElement descriptionButton;

    @FindBy(css = "[role='tabpanel']:nth-child(2) p ")
    @CacheLookup
    WebElement productDescriptionText;

    @FindBy(id = ".reviews_tab")
    @CacheLookup
    WebElement productReviewTab;

    @FindBy(css = "a.star-5 ")
    WebElement fiveStars;

    @FindBy(id = "comment")
    WebElement reviewDescription;

    @FindBy(id = "author")
    WebElement reviewAuthor;

    @FindBy(id = "email")
    WebElement reviewEmail;

    @FindBy(id = "submit")
    WebElement submitRewiewButton;

    @FindBy(css = "ol.commentlist > :last-child > div > div >div.description >p")
    WebElement lastReviewDescription;


    @FindBy(xpath = "//input[@name='quantity']")
    @CacheLookup
    WebElement productQuantity;

    @FindBy(css = ".single_add_to_cart_button")
    @CacheLookup
    WebElement addToCartButton;

    @FindBy(xpath = "//div[@class='woocommerce-message']")
    WebElement confirmationMessage;

    @FindBy(css = "[id='woocommerce_widget_cart-1'] .buttons .wc-forward:nth-of-type(1)")
    @CacheLookup
    WebElement viewCart;

    @FindBy(css = ".cart-subtotal")
    WebElement cartSubtotal;

    @FindBy(css = ".woocommerce-shipping-totals")
    WebElement cartShipping;

    @FindBy(css = ".order-total")
    WebElement cartTotal;

    @FindBy(linkText= "Proceed to checkout")
    @CacheLookup
    WebElement proceedToCheckout;

//    @FindBy(css = ".woocommerce .cart_item:nth-of-type(2) [aria-label]")
//    WebElement removeProduct;
//
//    @FindBy(css = ".woocommerce .woocommerce-message:nth-of-type(2)")
//    WebElement productRemovedMessage;
//
//    @FindBy(css = ".cart-empty")
//    @CacheLookup
//    WebElement emptyCartMessage;
//
//    @FindBy(linkText = "Undo?")
//    WebElement undoRemoveProduct;
//
//    @FindBy(css = ".wc-backward")
//    @CacheLookup
//    WebElement returnToShopButton;
//
//    @FindBy(css = "[id='woocommerce_widget_cart-1'] .checkout")
//    @CacheLookup
//    WebElement checkout;


    public String getProductTitle() {
        return productTitle.getText();
    }

    public String getProductCategory(){
        return productCategory.getText();
    }

    public void goToProductDescription(){
        descriptionButton.click();
    }

    public String getProductDescriptionText(){
        return productDescriptionText.getText();
    }

    public void addToCart(String quantity) {
        productQuantity.clear();
        productQuantity.sendKeys(quantity);
        addToCartButton.click();
    }

    public String getProductAddedToCartMessage() {
        return confirmationMessage.getText();
    }


    public String getProductQuantity() {
        return productQuantity.getAttribute("value");
    }



    public void addReview(String review, String name, String email) {
        productReviewTab.click();
        fiveStars.click();
        reviewDescription.sendKeys(review);
        reviewAuthor.sendKeys(name);
        reviewEmail.sendKeys(email);
        submitRewiewButton.click();
    }

    public String getLastReviewDescription() {
        return lastReviewDescription.getText();
    }

    public void navigateToCart() {
        viewCart.click();
    }

    public String getCartSubtotal(){
        return cartSubtotal.getText();
    }

    public String getShippingRate(){
        return cartShipping.getText();
    }

    public String getCartTotal(){
        return cartTotal.getText();
    }

    public MyAccountPageFactory goToCheckout(){
        proceedToCheckout.click();

        return new MyAccountPageFactory(driver);
    }

}



