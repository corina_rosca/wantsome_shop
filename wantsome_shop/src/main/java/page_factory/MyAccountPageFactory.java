package page_factory;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MyAccountPageFactory {
    private WebDriver driver;

    public MyAccountPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Registration Page
    @FindBy(id = "reg_email")
    @CacheLookup
    WebElement registrationEmailAddress;

    @FindBy(id = "reg_password")
    @CacheLookup
    WebElement registrationPassword;

    @FindBy(id = "registration_field_1")
    @CacheLookup
    WebElement firstName;

    @FindBy(id = "registration_field_2")
    @CacheLookup
    WebElement lastName;

    @FindBy(id = "registration_field_3")
    @CacheLookup
    WebElement phoneNumber;

    @FindBy(css = "[name='register']")
    @CacheLookup
    WebElement registerButton;

    @FindBy(css = "[id='site-title']")
    WebElement registrationPageTitle;

    @FindBy(css = "ul[role='alert']")
    @CacheLookup
    WebElement emptyEmailRegFieldError;

    @FindBy(xpath = "//ul[@class ='woocommerce-error']/li")
    @CacheLookup
    WebElement emptyPassRegError;

    @FindBy(css = ".woocommerce-error li")
    @CacheLookup
    WebElement alreadyRegisterEmail;


    //Login Page
    @FindBy(id = "username")
    WebElement usernameOrEmail;

    @FindBy(id = "password")
    WebElement password;

    @FindBy(css = "[name='login']")
    WebElement loginButton;

    @FindBy(id = "rememberme")
    WebElement rememberMeCheckBox;

    @FindBy(xpath = "//h2[@class='entry-title']")
    WebElement loginPageTitle;

    @FindBy(css = ".woocommerce-error li")
    @CacheLookup
    WebElement emptyMandatoryFieldsErrorMsg;

    @FindBy(css = ".woocommerce-error li")
    @CacheLookup
    WebElement emptyPassErrorMsg;

    @FindBy(css = ".woocommerce-error li")
    @CacheLookup
    WebElement unknownUsernameErrorMsg;

    @FindBy(css = ".woocommerce-error li")
    @CacheLookup
    WebElement unknownEmailErrorMsg;

    @FindBy(css = "[class*='lost_password'] a")
    WebElement lostYourpasswordLink;

    @FindBy(id = "user_login")
    WebElement lostYourUsername;

    @FindBy(css = "[class*='error'] li a")
    WebElement lostpasswordLinkFromMsg;

    @FindBy(className = "woocommerce-message")
    WebElement resetPassSucces;

    @FindBy(className = "woocommerce-error")
    WebElement resetPassError;

    @FindBy(css = "[class*='error'] li")
    WebElement errorMsg;


    //My account Webelements
    @FindBy(linkText = "Account details")
    @CacheLookup
    WebElement accountDetails;

    @FindBy(linkText = "Logout")
    WebElement logoutButton;

    @FindBy(id = "secondary")
    @CacheLookup
    List<WebElement> asideArea;

    @FindBy(id = "account_display_name")
    @CacheLookup
    WebElement display_name;


//   Registration steps

    public void registerAccount(String email, String pass, String f_Name, String l_Name, String phoneNo) {
        HomePageFactory homePage = new HomePageFactory(driver);
        homePage.navigateToLoginOrRegisterPage();
        registrationEmailAddress.sendKeys(email);
        registrationPassword.sendKeys(pass);
        firstName.sendKeys(f_Name);
        lastName.sendKeys(l_Name);
        phoneNumber.sendKeys(phoneNo);
        registerButton.click();
    }


    public String checkRegistrationPageTitle() {
        return registrationPageTitle.getText();

    }

    public String getEmptyEmailRegistrationError() {
        return emptyEmailRegFieldError.getText();
    }

    public String getAlreadyRegisteredEmail() {
        return alreadyRegisterEmail.getText();
    }

    public String getEmptyPassRegistrationError() {
        return emptyPassRegError.getText();
    }


    public void logoutBtn() {
        logoutButton.click();
    }


    //Login steps

    public void loginToMyAccount(String username, String pass) {
        HomePageFactory homePage = new HomePageFactory(driver);
        homePage.navigateToLoginOrRegisterPage();
        usernameOrEmail.sendKeys(username);
        password.sendKeys(pass);
        if (!rememberMeCheckBox.isSelected())
            rememberMeCheckBox.click();
        loginButton.click();

    }

    public String getLoginPageTitle() {
        return loginPageTitle.getText();
//        String loginTitle = driver.getTitle();
//        String loginPageTitle = "My account – Wantsome Shop";
//        Assert.assertTrue(loginTitle.equals(loginPageTitle));

    }

    public String getAccountDetails() {
        asideArea = driver.findElements(By.id("secondary"));
        return accountDetails.getText();
    }


    public void lostThePassword() {
        lostYourpasswordLink.click();

    }

    public void getlostPasswordFromErrorMessage() {
        lostpasswordLinkFromMsg.click();
    }


    public void setLostYourUsername(String username) {
        lostYourUsername.sendKeys(username);
        lostYourUsername.sendKeys(Keys.ENTER);
    }


    public String getpasswordResetSuccessfully() {
        String succesMsg = resetPassSucces.getText();
        return succesMsg;
    }

    public String getpasswordResetError() {
        String errorMsg = resetPassError.getText();
        return errorMsg;
    }

    public String getErrorMsg() {
        String errorMesage = errorMsg.getText();
        return errorMesage;
    }


    public String getMandatoryEmptyFieldsError() {
        return emptyMandatoryFieldsErrorMsg.getText();
    }

    public String getEmptyPassError() {
        return emptyPassErrorMsg.getText();
    }

    public String getUnkownUserError() {
        return unknownUsernameErrorMsg.getText();
    }

    public String getUnknownEmailError() {
        return unknownEmailErrorMsg.getText();
    }

    public String getAccountNavigation() {
        List<WebElement> accountNavigation = driver.findElements(By.cssSelector(".woocommerce-MyAccount-navigation"));
        for (WebElement account : accountNavigation) {
            return account.getText();


        }

        return null;
    }


}
