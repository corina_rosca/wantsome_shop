package page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartPageFactory {
    private WebDriver driver;

    public CartPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(css = ".quantity [step]")
    @CacheLookup
    WebElement inputQuantity;

    @FindBy(linkText = "Converse")
    @CacheLookup
    WebElement productAddedToCartName;


    @FindBy(id = "coupon_code")
    WebElement couponCode;

    @FindBy(xpath = "//button[@name=\"apply_coupon\"]")
    WebElement applyCouponButton;

    @FindBy(css = ".woocommerce-error li")
    WebElement errorCouponMsg;

    @FindBy(css = "[name='update_cart']")
    @CacheLookup
    WebElement updateCartButton;

    @FindBy(css = ".woocommerce .woocommerce-message:nth-of-type(1)")
    WebElement cartUpdatedMessage;


    @FindBy(css = ".checkout-button")
    WebElement checkoutButton;


    public String getCartProductQuantity() {
        return inputQuantity.getAttribute("value");
    }

    public String getCartProductName() {
        return productAddedToCartName.getText();
    }


    public void updateProductQuantity(String quantity) {
        inputQuantity.clear();
        inputQuantity.sendKeys(quantity);
        updateCartButton.click();
    }

    public String getUpdateQuantityMessage() {
        return cartUpdatedMessage.getText();
    }


    public String getCouponErrorMsg() {
        couponCode.click();
        couponCode.sendKeys("123");
        applyCouponButton.click();
        return errorCouponMsg.getText();
    }

    public void clickOnCheckoutButton() {
        checkoutButton.click();
    }


}
