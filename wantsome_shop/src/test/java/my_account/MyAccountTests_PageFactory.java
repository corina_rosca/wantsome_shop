package my_account;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import page_factory.HomePageFactory;
import page_factory.MyAccountPageFactory;
import utils.BaseTestClass;

import java.util.concurrent.TimeUnit;

public class MyAccountTests_PageFactory extends BaseTestClass {
    String registrationPageTitle = "Wantsome Shop – Wantsome? Come and get some!";
    String email_user = "userRegister" + RandomStringUtils.randomNumeric(3) + "@gmail.com";
    String pass = "MyWantsomePassword1!";
    String f_Name = "FirstName";
    String l_Name = "LastName";
    String phoneNo = "0770330000";
    String emptyRegistrationEmailError = "Error: Please provide a valid email address.";
    String emptyRegistrationPassField = "Error: Please enter an account password.";
    String alreadyRegisterEmail = "Error: An account is already registered with your email address. Please log in.";

    String accountPageTitle = "My account – Wantsome Shop";
    String accountTitle = "MY ACCOUNT";
    String username = "userRegister978@gmail.com";
    String password = "MyWantsomePassword1!";
    String accountDetails = "Account details";
    String logout = "Logout";
    String emptyUserMsgError = "Error: Username is required.";
    String emptyPassFieldMsgError = "ERROR: The password field is empty.";
    String unknownEmailMsgError = "Unknown email address. Check again or try your username.";
    String unregisteredUserError = "ERROR: The password you entered for the username testare is incorrect. Lost your password?";

    private MyAccountPageFactory navigateToLogin() {
        HomePageFactory homePage = new HomePageFactory(driver);
        MyAccountPageFactory accountPage = homePage.navigateToLoginOrRegisterPage();
        return accountPage;
    }


// Registration Tests

    @Before
    public void ImplicitWait() {
        driver.manage().timeouts().implicitlyWait(12000, TimeUnit.MILLISECONDS);
    }

    @Test
    public void validRegistration() {
        MyAccountPageFactory register = new MyAccountPageFactory(driver);
        register.registerAccount(email_user, pass, f_Name, l_Name, phoneNo);
        Assert.assertTrue(registrationPageTitle.contains(register.checkRegistrationPageTitle()));

    }


    @Test
    public void registerWithTheSameEmail() {
        MyAccountPageFactory register = new MyAccountPageFactory(driver);
        register.registerAccount("userRegister978@gmail.com", pass, f_Name, l_Name, phoneNo);
        Assert.assertEquals(alreadyRegisterEmail, register.getAlreadyRegisteredEmail());
    }


    @Test
    public void registerWithoutEmail() {
        MyAccountPageFactory register = new MyAccountPageFactory(driver);
        register.registerAccount("", pass, f_Name, l_Name, phoneNo);
        Assert.assertEquals(emptyRegistrationEmailError, register.getEmptyEmailRegistrationError());
    }

    @Test
    public void registerWithoutPass() {
        MyAccountPageFactory register = new MyAccountPageFactory(driver);
        register.registerAccount(email_user, "", f_Name, l_Name, phoneNo);
        Assert.assertEquals(emptyRegistrationPassField, register.getEmptyPassRegistrationError());
    }


    //  LOGIN tests
    @Test
    public void validLogin() {
        MyAccountPageFactory loginPage = new MyAccountPageFactory(driver);
        loginPage.loginToMyAccount(username, password);
        Assert.assertEquals(accountTitle, loginPage.getLoginPageTitle());
        Assert.assertTrue(loginPage.getAccountNavigation().contains(accountDetails));
        Assert.assertTrue(loginPage.getAccountNavigation().contains(logout));

        loginPage.logoutBtn();
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        Assert.assertTrue(homePageFactory.checkHomePageTitle().equals(driver.getTitle()));
        Assert.assertTrue(driver.getTitle().equals(accountPageTitle));


    }

    @Test
    public void checkAccountDetailsLink() {
        MyAccountPageFactory loginPage = navigateToLogin();
        loginPage.loginToMyAccount(username, password);
        Assert.assertTrue(loginPage.getAccountDetails().equals("Account details"));
    }


    @Test
    public void resetPasswordWithUser() {
        MyAccountPageFactory accountPageFactory = navigateToLogin();
        accountPageFactory.lostThePassword();
        accountPageFactory.setLostYourUsername(username);
        Assert.assertEquals("Password reset email has been sent.", accountPageFactory.getpasswordResetSuccessfully());
    }

    @Test
    public void resetPasswordWithEmail() {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        MyAccountPageFactory accountPage = homePageFactory.navigateToLoginOrRegisterPage();
        accountPage.lostThePassword();
        accountPage.setLostYourUsername(username);
        Assert.assertTrue(accountPage.getpasswordResetSuccessfully().equals("Password reset email has been sent."));
    }

    @Test
    public void resetPasswordSuccessfullyFromErrorMessage() {
        MyAccountPageFactory accountPage = new MyAccountPageFactory(driver);
        accountPage.loginToMyAccount(username, "abcdef");
        String errorMessage = "ERROR: The password you entered for the email address " + username + " is incorrect. Lost your password?";
        Assert.assertEquals(errorMessage, accountPage.getErrorMsg());
        accountPage.getlostPasswordFromErrorMessage();
        accountPage.setLostYourUsername(username);
        Assert.assertEquals("Password reset email has been sent.", accountPage.getpasswordResetSuccessfully());
    }

    @Test
    public void resetPasswordWithoutEmail() {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        MyAccountPageFactory accountPage = homePageFactory.navigateToLoginOrRegisterPage();
        accountPage.lostThePassword();
        accountPage.setLostYourUsername("");
        Assert.assertEquals("Enter a username or email address.", accountPage.getpasswordResetError());
    }

    @Test
    public void resetPasswordWithInvalidCredentials() {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        MyAccountPageFactory accountPage = homePageFactory.navigateToLoginOrRegisterPage();
        accountPage.lostThePassword();
        accountPage.setLostYourUsername("mimimi");
        Assert.assertEquals("Invalid username or email.", accountPage.getpasswordResetError());
    }


    @Test
    public void loginWithEmptyFields() {
        MyAccountPageFactory loginPage = new MyAccountPageFactory(driver);
        loginPage.loginToMyAccount("", "");
        Assert.assertEquals(emptyUserMsgError, loginPage.getMandatoryEmptyFieldsError());
        loginPage.getLoginPageTitle();
        Assert.assertTrue(loginPage.getLoginPageTitle().equals("MY ACCOUNT"));


    }

    @Test
    public void loginWithEmptyPasswordField() {
        MyAccountPageFactory loginPage = new MyAccountPageFactory(driver);
        loginPage.loginToMyAccount(username, "");
        Assert.assertEquals(emptyPassFieldMsgError, loginPage.getEmptyPassError());
    }

    @Test
    public void loginWithEmptyEmailField() {
        MyAccountPageFactory loginPage = new MyAccountPageFactory(driver);
        loginPage.loginToMyAccount("", password);
        Assert.assertEquals(emptyUserMsgError, loginPage.getMandatoryEmptyFieldsError());
    }

    @Test
    public void loginWithUnknownEmail() {
        MyAccountPageFactory loginPage = new MyAccountPageFactory(driver);
        loginPage.loginToMyAccount("userRegister333@gmail.com", password);
        Assert.assertEquals(unknownEmailMsgError, loginPage.getUnknownEmailError());
    }

    @Test
    public void loginWithUnregisteredUser() {
        MyAccountPageFactory loginPage = new MyAccountPageFactory(driver);
        loginPage.loginToMyAccount("testare", "automata");
        Assert.assertEquals(unregisteredUserError, loginPage.getUnkownUserError());
    }


}
