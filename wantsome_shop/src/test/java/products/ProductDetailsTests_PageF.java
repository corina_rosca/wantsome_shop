package products;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import page_factory.HomePageFactory;
import page_factory.ProductsDetails;
import page_factory.ProductsSelection;
import utils.BaseTestClass;

import java.util.concurrent.TimeUnit;

public class ProductDetailsTests_PageF extends BaseTestClass {
    String quantity = "3";
    String review = "Review " + RandomStringUtils.randomNumeric(3);
    String name = "User" + RandomStringUtils.randomNumeric(2);
    String email = "usermail" + RandomStringUtils.randomNumeric(3) + "@gmail.com";
    String descriptionText = "Lorem Ipsum is simply dummy text of the printing and typesetting indu-stry." +
                             " Lorem Ipsum has been the industry’s standard.";

    @Before
    public void waitForElements() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @Test
    public void addProductsToCart() {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.clickOnProduct(1);

        ProductsDetails product = new ProductsDetails(driver);
        product.getProductQuantity();
        Assert.assertTrue(product.getProductQuantity().equals("1"));
        product.addToCart(quantity);

        String expectedConfirmationMessage = "View cart\n" + quantity + " × “" +
                product.getProductTitle() + "” have been added to your cart.";
        Assert.assertEquals(expectedConfirmationMessage, product.getProductAddedToCartMessage());

    }


    @Test
    public void getProductDetails(){
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.clickOnProduct(1);
        ProductsDetails product = new ProductsDetails(driver);
        product.getProductCategory();
        Assert.assertTrue(product.getProductCategory().contains("Men Collection"));
        product.goToProductDescription();
        product.getProductDescriptionText();
        Assert.assertEquals(descriptionText,product.getProductDescriptionText());
    }

    @Test
    public void addAReviewTest() {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.clickOnProduct(1);
        ProductsDetails product = new ProductsDetails(driver);
        product.addReview(review, name, email);
        Assert.assertEquals(review, product.getLastReviewDescription());
    }
}
