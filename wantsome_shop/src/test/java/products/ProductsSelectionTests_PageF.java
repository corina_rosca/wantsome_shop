package products;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import page_factory.HomePageFactory;
import page_factory.ProductsDetails;
import page_factory.ProductsSelection;
import utils.BaseTestClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProductsSelectionTests_PageF extends BaseTestClass {
    String option2 = "Sort by popularity";
    String option5 = "Sort by price: low to high";
    String myProduct = "Jeans";


    @Before
    public void waitForElements() {
        driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);

    }


    @Test
    public void sortProductsByPopularity_MenCollection() {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.sortBy(option2);
        Assert.assertEquals(myProduct, products.getProductsTitle(0));
    }

    @Test
    public void clickOnAProductMenCollection() {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.clickOnProduct(2);
        ProductsDetails product = new ProductsDetails(driver);
        Assert.assertEquals(myProduct, product.getProductTitle());

    }

    @Test
    public void sortProductByLowerPriceTest() {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.navigateToWomenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.sortBy(option5);
        List<Double> actualProductPrices = products.listOfPricesDigitsOnly(
                products.getProductsPrices());
        List<Double> expectedProductPrices = products.orderByLowerPrice(
                actualProductPrices);
        boolean resultOfComparison = products.compareListElementsOnTheSamePosition(
                actualProductPrices, expectedProductPrices);
        Assert.assertTrue(resultOfComparison);
    }


}
