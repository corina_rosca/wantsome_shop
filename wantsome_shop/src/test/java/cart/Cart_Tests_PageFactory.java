package cart;

import org.junit.Assert;
import org.junit.Test;
import page_factory.CartPageFactory;
import page_factory.HomePageFactory;
import page_factory.ProductsDetails;
import page_factory.ProductsSelection;
import utils.BaseTestClass;

public class Cart_Tests_PageFactory extends BaseTestClass {
    String initialQuantity = "1";
    String updatedQuantity = "5";
    String cartUpdatedMessage = "Cart updated.";
    String quantity = "4";

//“Converse” has been added to your cart.

    @Test

    public void updateCartQuantity() throws InterruptedException {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        // homePageFactory.checkHomePageLogoText();
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.clickOnProduct(1);
        Thread.sleep(5000);
        ProductsDetails product = new ProductsDetails(driver);
        product.addToCart(initialQuantity);
        Assert.assertTrue(product.getProductAddedToCartMessage().endsWith("has been added to your cart."));
        product.navigateToCart();

        CartPageFactory cart = new CartPageFactory(driver);
        Assert.assertTrue(cart.getCartProductName().equals("Converse"));

        cart.getCouponErrorMsg();
        Assert.assertTrue(cart.getCouponErrorMsg().startsWith("Coupon"));
        Assert.assertTrue(cart.getCouponErrorMsg().equals("Coupon \"123\" does not exist!"));

        cart.updateProductQuantity(updatedQuantity);
        Assert.assertEquals(cartUpdatedMessage, cart.getUpdateQuantityMessage());
    }

    @Test
    public void checkCartTotals() throws InterruptedException {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        // homePageFactory.checkHomePageLogoText();
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.clickOnProduct(1);
        Thread.sleep(5000);
        ProductsDetails product = new ProductsDetails(driver);
        product.addToCart(initialQuantity);
        product.navigateToCart();
        product.getCartSubtotal();
        Assert.assertTrue(product.getCartSubtotal().contains("25,00"));
        product.getShippingRate();
        Assert.assertTrue(product.getShippingRate().contains("Flat rate:"));
        product.getCartTotal();
        Assert.assertTrue(product.getCartTotal().contains("50,00"));
        product.goToCheckout();


    }

    @Test
    public void checkoutTest() {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.clickOnProduct(2);
        ProductsDetails product = new ProductsDetails(driver);
        product.addToCart(quantity);
        product.navigateToCart();
        CartPageFactory cart = new CartPageFactory(driver);
        String prodQuantity = cart.getCartProductQuantity();
        Assert.assertEquals(quantity, prodQuantity);
        cart.clickOnCheckoutButton();
        String actualPageTitle = driver.getTitle();
        Assert.assertEquals("Checkout – Wantsome Shop", actualPageTitle);
    }


}
