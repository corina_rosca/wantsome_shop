package cart;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import page_objects.CartPage;
import page_objects.CategoryPage;
import page_objects.ShopHomePage;
import utils.BaseTestClass;

public class CartTests extends BaseTestClass {

    @Test
    public void checkEmptyCart() {
        //click on Cart tab
        driver.findElement(By.xpath("//a[text()='Cart']")).click();

        //check Cart page title
        Assert.assertEquals("CART", driver.findElement(By.className("entry-title")).getText());

        //check Cart page message
        Assert.assertEquals("Your cart is currently empty.", driver.findElement(By.className("cart-empty")).getText());
    }

    @Test
    public void checkEmptyCartWithPageObject(){
        ShopHomePage shopHomePage = new ShopHomePage(driver);
        CartPage cartPage = shopHomePage.clickOnCart();

        Assert.assertEquals("CART",cartPage.getCartTitle());
        Assert.assertEquals("Your cart is currently empty.",cartPage.getCartMessage());

    }

    @Test
    public void checkCartValue(){
        ShopHomePage shopHomePage = new ShopHomePage(driver);
        shopHomePage.clickOnCategory();
        CategoryPage categoryPage = shopHomePage.clickOnMenCollection();
        categoryPage.addToCart();
        CartPage cartPage = new CartPage(driver);
        Assert.assertEquals(1,cartPage.getCartValue());

    }

}
