package search;

import org.junit.Assert;
import org.junit.Test;
import page_factory.HomePageFactory;
import page_factory.SearchResultPageFactory;
import utils.BaseTestClass;

public class SearchResultPageTests_PageF extends BaseTestClass {

    @Test
    public void checkSearchResults() throws InterruptedException {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.clickOnSearchIcon();
        homePageFactory.typeSearch("shoe");
        SearchResultPageFactory searchResult = new SearchResultPageFactory(driver);

        Assert.assertEquals(searchResult.checkResultNo("shoe"), 3);
        Assert.assertTrue(searchResult.checkSearchResult("shoe"));

    }
}
