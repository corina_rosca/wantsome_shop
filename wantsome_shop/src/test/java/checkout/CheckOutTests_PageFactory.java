package checkout;

import org.junit.Assert;
import org.junit.Test;
import page_factory.CheckOutPageFactory;
import page_factory.HomePageFactory;
import page_factory.ProductsDetails;
import page_factory.ProductsSelection;
import utils.BaseTestClass;

public class CheckOutTests_PageFactory extends BaseTestClass {
    String initialQuantity = "1";


//    private MyAccountPageFactory navigateToCheckOut() {
//        HomePageFactory homePageFactory = new HomePageFactory(driver);
//        homePageFactory.navigateToMenCollection();
//        ProductsSelection products = new ProductsSelection(driver);
//        products.clickOnProduct(1);
//        ProductsDetails product = new ProductsDetails(driver);
//        product.addToCart(initialQuantity);
//        product.navigateToCart();
//        product.goToCheckout();
//        MyAccountPageFactory checkOutPage = product.goToCheckout();
//        product.goToCheckout();
//        return checkOutPage;
//    }

    @Test
    public void checkPageTitle() throws InterruptedException {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.clickOnProduct(1);
        ProductsDetails product = new ProductsDetails(driver);
        product.addToCart(initialQuantity);
        product.navigateToCart();
        product.goToCheckout();
        CheckOutPageFactory checkout = new CheckOutPageFactory(driver);

        checkout.getPageTitle();
        Assert.assertTrue(checkout.getPageTitle().equals("CHECKOUT"));
        checkout.getLoginForm();
        checkout.getUsernameLabel();
        Thread.sleep(2000);
        Assert.assertTrue(checkout.getUsernameLabel().startsWith("Username"));
        checkout.getCurrentPassLabel();
        Assert.assertTrue(checkout.getCurrentPassLabel().contains("Password"));


    }

    @Test
    public void checkCouponCode() throws InterruptedException {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.clickOnProduct(1);
        ProductsDetails product = new ProductsDetails(driver);
        product.addToCart(initialQuantity);
        product.navigateToCart();
        product.goToCheckout();
        CheckOutPageFactory checkout = new CheckOutPageFactory(driver);
        checkout.getHaveACouponCode();
        checkout.getCouponInputText();
        checkout.getCouponErrorMsg();
        Assert.assertTrue(checkout.getCouponErrorMsg().equals("Coupon \"123ab\" does not exist!"));
    }

    @Test
    public void checkPlaceTheOrderWithoutMandatoryFields() throws InterruptedException {
        HomePageFactory homePageFactory = new HomePageFactory(driver);
        homePageFactory.navigateToMenCollection();
        ProductsSelection products = new ProductsSelection(driver);
        products.clickOnProduct(1);
        ProductsDetails product = new ProductsDetails(driver);
        product.addToCart(initialQuantity);
        product.navigateToCart();
        product.goToCheckout();
        CheckOutPageFactory checkout = new CheckOutPageFactory(driver);
        checkout.placeTheOrder();
        checkout.getCheckOutError();
        Assert.assertTrue(checkout.getCheckOutError().contains("Billing First name is a required field."));
        Assert.assertTrue(checkout.getCheckOutError().endsWith("Shipping Postcode / ZIP is a required field."));

    }
}
